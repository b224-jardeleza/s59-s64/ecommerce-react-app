import { useState, useEffect, useContext} from 'react';
import { Button, Container, Table, Row, Col } from 'react-bootstrap';
import { Link, useParams, useNavigate }  from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function AllOrders(){

	const { user } = useContext(UserContext);
	const { orderId } = useParams();

	const navigate = useNavigate();

	const [orders, setOrders] = useState();
	const [product, setProduct] = useState();
	const [productName, setProductName] = useState();
	const [unitPrice, setPrice] = useState();
	const [quantity, setQuantity] = useState();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/orders/all`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			const sortOrders = data.sort((a, b) => (a.userId > b.userId) ? 1 : -1);
			setOrders(sortOrders);
		})
	}, [setOrders])


	return (

			<Table hover bordered>
				<thead>
					<tr>
						<th>User ID</th>
			            <th>Order No.</th>			            
			            <th>Products</th>
			            <th>Status</th>
			        </tr>
				</thead>
				<tbody>
					{
						orders && orders.map(order => (
		                    <tr key={order.userId}>			                    
		                        <td>{order.userId}</td>
		                        <td>{order._id}</td>
		                        <td>
		                        	<Table bordered>
		                        		<thead>
		                        			<tr>
                    			                <th>Product ID</th>
                    			                <th>Quantity</th>
                    			                <th>Subtotal</th>
                    			              </tr>
		                        		</thead>
		                        	
		                        	<tbody>
		                        		{
		                        		    order.products.map(product => (
	                        		           	<tr key={product._id} onClick={() => setProduct(product)}>
	                        		           	    <td className="text-center">{product.productId}</td>
	                        		           	    <td className="text-center">{product.quantity}</td>
	                        		           	    <td className="text-center">{product.subtotal}</td>
	                        		           	</tr>
		                        		    ))


		                        		}
		                        		<tr>
                    		                <td className="text-center">Total Amount</td>
                    		                <td className="text-center" colSpan={3}>{order.products.reduce((acc, curr) => acc + curr.subtotal, 0)}</td>
                    		            </tr>
		                        	</tbody>
		                        	</Table>
		                        </td>
		                        <td>
		                        	{(order.isCheckedOut === false) ?
		                        		'For check out'
		                        		:
		                        		'To ship'
		                        	}
		                        </td>
		                        
		                    </tr>
			            ))
					}
				</tbody>
			</Table>
	
	)
}