import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights(){
	return (


	    <Row className="mt-3 mb-3">
	    	<Col xs={12} md={4}>
	    		<Card className="cardHighlight p-3">
	    		<Card.Img variant="top" src="https://i.pinimg.com/474x/43/18/42/431842f43383db788c842a24fb4b7c26.jpg" />
	    		  <Card.Body>
	    		    <Card.Title>Dress</Card.Title>
	    		    <Card.Text>
	    		      Dresses for those who like to standout in the crowd.
	    		    </Card.Text>
	    		  </Card.Body>
	    		</Card>
	    	</Col>

	    	<Col xs={12} md={4}>
	    		<Card className="cardHighlight p-3">
	    		<Card.Img variant="top" src="https://lzd-img-global.slatic.net/g/p/3115429402e0fbae0cdccf0dc004350d.jpg" />
	    		  <Card.Body>
	    		    <Card.Title>Pants</Card.Title>
	    		    <Card.Text>
	    		      Pants for women who wants to show dominance with femininity.
	    		    </Card.Text>
	    		  </Card.Body>
	    		</Card>
	    	</Col>

	    	<Col xs={12} md={4}>
	    		<Card className="cardHighlight p-3">
	    		<Card.Img variant="top" src="https://zoodmall.com/cdn-cgi/image/w=600,fit=contain,f=auto/https://images.zoodmall.com/cloudinary/e4/a6d/https%253A%252F%252Fae01.alicdn.com%252Fkf%252FH562234e097f04a1999457dfcb546f522b%252FElastic-High-Waist-Sexy-Patchwork-White-Cascading-Ruffles-Women-Midi-Long-Skirt-Korean-Vintage-Kawaii-Sweet.jpg" />
	    		  <Card.Body>
	    		    <Card.Title>Skirts</Card.Title>
	    		    <Card.Text>
	    		      Skirts for those who like to show their sweet side.
	    		    </Card.Text>
	    		  </Card.Body>
	    		</Card>
	    	</Col>
	    </Row>


	);
}