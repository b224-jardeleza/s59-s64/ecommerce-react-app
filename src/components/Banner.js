import { useState, useEffect, useContext } from 'react';
import {Row, Col, Button, Table} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Banner({data}) {

	const { user, setUser } = useContext(UserContext);

	const {title, content} = data;

	return (

		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				{
					(user.id !== null && user.isAdmin === true) ?
					<>
						<Button variant="outline-primary" as={Link} to={`/addproduct`}>Add Product</Button>
						<Button variant="outline-primary" as={Link} to={`/orders`}>See all orders</Button>
					</>
					:
					<p></p>
				}
				{
					(user.id == null) ?
					 <>
					 	<Button variant="outline-primary" as={Link} to={`/products`}>See our products</Button>
					 </>
					 :
					 <p></p>
				}
			</Col>
		</Row>
	)
}
