import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, Image, Container } from 'react-bootstrap';
import { Link }  from 'react-router-dom';

export default function ProductCard({productProp}) { 


  const { productName, productDescription, unitPrice, category, productImage, _id } = productProp;

  return (

    <Container className="border rounded mt-3 mb-3" md={4}>
        <Row className="mt-3 mb-3">
          <Col xs={12} md={5} lg={8} className="mx-5 align-self-center">
            {/*<Col>           */}
                    <Card.Body>
                      {/*<Card.Row>*/}
                        <Card.Title><h2>{productName}</h2></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{productDescription}</Card.Text>
                        <Card.Subtitle>Category</Card.Subtitle>
                        <Card.Text>{category}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>Php {unitPrice}</Card.Text>
                        <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
                        {/*<Image src={productImage}/>*/}
                      {/*</Card.Row>*/}
                    </Card.Body>  
            {/*</Col>*/}
            
          </Col>
          <Col xs={12} md={5} lg={2} className="mt-3 mb-3 align-self-center">
            <Card.Body>
                  <Image src={productImage} id="productImage"/>
            </Card.Body>
          </Col>
        </Row>
    </Container>
  )
}