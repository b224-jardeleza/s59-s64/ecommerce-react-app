import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){


	const { user } = useContext(UserContext);
	return (

	  <Navbar bg="light" expand="lg" sticky="top">
	    <Container>
	      <Navbar.Brand as={Link} to="/">Sew and Style</Navbar.Brand>
	      <Navbar.Toggle aria-controls="basic-navbar-nav" />
	      <Navbar.Collapse id="basic-navbar-nav">
	        <Nav>
	          <Nav.Link as={Link} to="/">Home</Nav.Link>

	          {
	          	(user.isAdmin == true) ?
	          	<Nav.Link as={Link} to="/dashboard">Dashboard</Nav.Link>
	          	:
	          	<>
		          	<Nav.Link as={Link} to="/products">Products</Nav.Link>
	          	</>
	          }     

	          {
	          	(user.id != null && user.isAdmin == false) ?	          	
	          		<Nav.Link as={Link} to="/orders">Cart</Nav.Link>
	          	:
	          	<></>
	          }

	          {
	          	(user.id != null) ?
          		<>
          			<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
          		</>
          		:
          		<>
          			<Nav.Link as={Link} to="/login">Login</Nav.Link>
          			<Nav.Link as={Link} to="/register">Register</Nav.Link>
          		</>
	          }
	        </Nav>
	      </Navbar.Collapse>
	    </Container>
	  </Navbar>

	);
}