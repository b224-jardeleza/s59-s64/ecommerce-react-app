import React from 'react';

// Create a context object
// A context object as the name suggest is a data type of an object that can be used to store information that can be shared to other components within the app.
// Context object is a different approach in passing information between components and allows easier access by avoiding props-drilling.
const UserContext = React.createContext();

// The provider component allows other components to comsume/use the context object and supply the necessary information needed to the context object.
// prepare the package
export const UserProvider = UserContext.Provider;

// open package for child components
export default UserContext;