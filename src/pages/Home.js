import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Sew and Style Boutique",
		content: "Don't force your body to fit a new dress. Let your dress fits you!"
	}


	return (

		<>
			<Banner data={data} />
			<Highlights />
		</>

	)
}
