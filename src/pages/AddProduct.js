import { useState, useEffect, useContext } from 'react';
import { Button, Table, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct(){

	const { user, setUser } = useContext(UserContext);

	const [productName, setProductName] = useState(''); 
	const [productDescription, setDescription] = useState(''); 
	const [unitPrice, setPrice] = useState(''); 
	const [category, setCategory] = useState(''); 

	const [isActive, setIsActive] = useState(false);
	
	const navigate = useNavigate()

function addNewProduct(e) {
	e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/add-product`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				productDescription: productDescription,
				unitPrice: unitPrice,
				category: category
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === false) {
				Swal.fire({
					title: "Duplicate product found!",
					icon: "error"
				})	

			} else {
				Swal.fire({
					title: "New product is succesfully added!",
					icon: "success"
				})		

				navigate('/dashboard')	
			}
		})
	
	setProductName("");
	setDescription("");
	setPrice("");
	setCategory("");
}

	useEffect(() => {
		if(productName !== "" && productDescription !== "" && unitPrice !== "" && category !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

		
	}, [productName, productDescription, unitPrice, category])

	return (

		<Form onSubmit={e => addNewProduct(e)}>
		      <Form.Group className="mb-3" controlId="productName">
		      	<Form.Label>Product Name</Form.Label>
		      	<Form.Control
		      		type="text" 
		      		placeholder="Enter product name"
		      		value={productName}
		      		onChange={e => setProductName(e.target.value)}
		      		required
		      	/>
		      	</Form.Group>	

		      	<Form.Group className="mb-3" controlId="productDescription">
		      	<Form.Label>Description</Form.Label>
		      	<Form.Control
		      		as="textarea" 
		      		placeholder="Enter description"
		      		value={productDescription}
		      		onChange={e => setDescription(e.target.value)}
		      		required
		      		style={{ height: '100px' }}
		      	/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="unitPrice">  		
		        <Form.Label>Unit Price</Form.Label>
		        <Form.Control 
		        	type="number" 
		        	placeholder="Enter Price"
		        	value={unitPrice}
		        	onChange={e => setPrice(e.target.value)}
		        	required
		        />
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="productCategory">  		
		        <Form.Label>Category</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter category"
		        	value={category}
		        	onChange={e => setCategory(e.target.value)}
		        	required
		        />
		      	</Form.Group>
			      	{
				      	isActive ?
				      		<Button variant="primary" type="submit" id="submitBtn">
				      		  Add
				      		</Button>
				      		:
				      		<Button variant="primary" type="submit" id="submitBtn" disabled>
				      		  Add
				      		</Button>
				    }
			      		<Button variant="danger" type="cancel" id="submitBtn">
			      		  Cancel
			      		</Button>
			    </Form>
	)
}