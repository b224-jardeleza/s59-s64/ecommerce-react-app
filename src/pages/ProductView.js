import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const addOrder = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/order-product`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
					title: "Successfully added to your cart",
					icon: "success"
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	};

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.productName);
			setDescription(data.productDescription);
			setPrice(data.unitPrice);
			setImage(data.productImage);
		})

	}, [productId])

	return (

		<Container>
    		<Row>
    		  <Col lg={{span: 6, offset: 3}}>
    		        <Card>
    		        	<Card.Img variant="top" src={image} />
    		              <Card.Body className="text-center">
    		                <Card.Title><h2>{name}</h2></Card.Title>
    		                <Card.Subtitle>Description</Card.Subtitle>
    		                <Card.Text>{description}</Card.Text>
    		                <Card.Subtitle>Price</Card.Subtitle>
    		                <Card.Text>Php {price}</Card.Text>
    		                <Form>
    		                	<Form.Label>Category</Form.Label>
    		                	<Form.Control 
    		                		type="text" 
    		                		placeholder="Enter quantity"
    		                		value={quantity}
    		                		onChange={e => setQuantity(e.target.value)}
    		                		required
    		                	/>
    		                </Form>
    		                {
    		                	(user.id !== null) ?
    		                		<Button variant="primary" onClick={() => addOrder(productId)}>Add to Cart</Button>
    		                	:
    		                	<Button className="btn btn-danger" as={Link} to="/login">Log in to add this to your cart</Button>
    		                }

    		              </Card.Body>
    		        </Card>
    		  </Col>
    		</Row>
		</Container>
	)
}