import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

import Banner from '../components/Banner';
import ProductTable from '../components/ProductTable';



export default function Dashboard() {

	const data = {
		title: "Admin Dashboard",
		content: ""
	}

	return (

		<>
			<Banner data={data} />
			
			<ProductTable />
		</>

	)

}