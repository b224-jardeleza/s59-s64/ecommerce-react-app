import Banner from '../components/Banner';


export default function Error() {

	const data = {
		title: "Error 404 - Page Not Found",
		content: "The page you are looking for cannot be found"
	}

	return (
		<Banner data={data}/>
	)

}
